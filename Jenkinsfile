#!/usr/bin/env groovy

// COMPLETE CI/CD PIPELINE USING DOCKER COMPOSE TO DEPLOY ON EC2 REMOTE SERVER

// This was used for Java Maven app built in Module 8 demos. Made Module 8 demos DockerHub repo private for this pipeline build.

// this import method means the JSL is project-scoped, not globally available to all Jenkins pipelines
// updated to use own GL repo URL and Jenkins-configured GL creds
library identifier: 'jenkins-shared-library-aws@main', retriever: modernSCM(
    [$class: 'GitSCMSource',
    remote: 'https://gitlab.com/twndevops/jenkins-shared-library-aws.git',
    credentialsID: 'git-creds'
    ]
)

pipeline {
    agent any
    tools {
        // applied Maven installation
        maven 'maven-3.9.5'
    }
    // environment {
    //     // Module 8 demos DockerHub repo (made private for this build)
    //     IMAGE_NAME = 'upnata/module-8-java-mvn-app:2.1-module-9'
    // }
    stages {
        stage('increment app version') {
            steps {
                script {
                    echo 'incrementing minor version'
                    // update pom.xml version
                    // versions:commit doesn't actually Git commit the change! Done in last stage
                    sh "mvn build-helper:parse-version versions:set \
                        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.nextMinorVersion}.\\\${parsedVersion.incrementalVersion} \
                        versions:commit"
                    def matches = readFile('pom.xml') =~ '<version>(.+)</version>'
                    echo "matches[0] = ${matches[0]}"
                    def version = matches[0][1]
                    // set env var -> pass to script file -> export to Compose YAML
                    env.IMAGE_NAME = "upnata/module-8-java-mvn-app:$version-$BUILD_NUMBER"
                }
            }
        }
        stage('build app') {
            steps {
                buildJar()
            }
        }
        stage('build image') {
            steps {
                script {
                    // Groovy file is buildImage, lib func is buildDockerImage
                    buildImage(env.IMAGE_NAME)
                    dockerLogin()
                    dockerPush(env.IMAGE_NAME)
                }
            }
        } 
        stage("deploy") {
            steps {
                script {
                    echo 'composing up Docker network on EC2...'
                    def ec2endpt = "ec2-user@54.145.246.218"
                    def bashCmds = "bash ./ec2-cmds.sh ${IMAGE_NAME}"
                    // use your EC2 ssh private key credential configured in Jenkins specifically for the aws-multibranch-pipeline build
                    sshagent(['ec2-server-key']) {
                        // copy bash script file and Docker Compose YAML from Jenkins' locally checked out repo to EC2 server working dir (cmd pwd)
                        // need permissions to scp into remote EC2 (must run in sshagent block)
                        sh "scp ec2-cmds.sh ${ec2endpt}:/home/ec2-user"
                        // running next cmd in bash script file fails b/c no longer on Jenkins server (which has Compose file); EC2 doesn't have any Compose file so the cmd doesn't work
                        sh "scp docker-compose.yaml ${ec2endpt}:/home/ec2-user"
                        // use your EC2 public IP
                        sh "ssh -o StrictHostKeyChecking=no ${ec2endpt} ${bashCmds}"
                    }
                }
            }               
        }
        stage('commit version update') {
            steps {
                script {
                    echo 'committing version update...'
                    // configure Jenkins user's name and email for Git commit to work
                    sh "git config user.name 'jenkins'"
                    sh "git config user.email 'jenkins@example.com'"
                    // confirm name and email added to config list, version update is committed, and our branch name (Jenkins checks out latest commit hash of remote repo, not branch specifically)
                    sh 'git config --list && git status && git branch'
                    // connect Jenkins' locally checked out repo to our remote repo
                    withCredentials([usernamePassword(credentialsId: 'git-creds', usernameVariable: 'USER', passwordVariable: 'PWD')]) {
                        // following cmd connects two repos and authenticates Jenkins to GL
                        sh "git remote set-url origin https://${USER}:${PWD}@gitlab.com/twndevops/java-maven-app-aws.git"
                    }
                    sh "git add pom.xml"
                    sh "git commit -m'ci: minor version bump'"
                    sh "git push origin HEAD:${BRANCH_NAME}"
                }
            }
        }
        // pull changes after every build!
    }
}
